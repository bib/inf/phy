# Documentation des chantiers au BIB

Voir les [tickets ouverts](https://framagit.org/bib/inf/phy/-/issues) pour une liste des travaux en cours et des tâches ouvertes.


## Mur de séparation avec l'appenti

Voir [./separation-appenti](./separation-appenti).

## Chantier électrique

Voir [./chantier-elec](./chantier-elec).
