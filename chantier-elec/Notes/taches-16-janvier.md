# Préparation des commandes

- Identifier les arrivées éclairage ou du 5G1.5mm2 est nécessaire (là ou il y a des BAES), puis réporter sur le tableur
- Métrage des câbles en place
- Saisir le métrage du stock réalisée avec Lou le 16/01/23
- Métrage des besoins en câbles, circuit par circuit
- Regarder si on a assez de fil 6mm2 pour faire les cordons dans les tableaux électriques,
  et sinon ajouter aux commandes 
- Identifier le matériel pour poser le tableau sur le nouveau mur en Siporex
  + Idéalement, faudrait des vis/chevilles assez longues pour aller chercher le béton derrière le siporex (~15cm)
  + On peut même déjà poser les chevilles, en regardant le gabarit du tableau 4x18 dans le devis
    (c'est dans la fiche technique du tableau, disponible auprès du fabriquant)
- Trouver des borniers de terre compatibles avec les tableaux, sauf s'ils sont déjà fournis
  avec les tableaux qu'on commande (a voir)

# Commandes et trucs à choper

- Commande matériel auxiliaire
  + Rubans de Dymo
  + Gaine thermorétractable adaptée pour du cable électrique (10 et 20 mm2 de diamètre avant rétreint)
  + Cosses pour tableau électrique et pince à sertir
- A ajouter à commande Yess
  + Matériel pour liaisons équipotentielles (voir GitLab)
  + Wagos (voir GitLab)
  + Borniers (1P, 1N) pour le cablage 24VDC des BAES
  + Remplacer certains répartiteurs 4P par des répartiteurs 2P, pour les sous-tableaux en mono


# Installation matériel/équipements

- Pose des rails, mais pour ça voir ticket dédié sur GitLab
  + il y a les instructions et tout
- Préparer les passages de câbles en salle serveurs (tuyau PVC + mousse expansible à poser à la fin)
- Préparer les passages de câbles entre A1 et B1 (tuyau PVC + mousse expansible à poser à la fin)
- Retirer le tuyau d'alimentation en eau du Labo, qui part de la brasserie
  + Nécessite un coude pour tuyau multicouche (à compression!)


# Mises à jour des données de travail

- Mises à jour de la liste des circuits :
  + GEN-VMC, départ tableau GEN, arrivée A1, 1.5mm2 (10A)
  + SRV-VMC, départ tableau SRV, arrivée C4, 1.5mm2 (10A)
- Mises à jour du nombre de prises par circuit
- Voir avec Adri pour savoir ou se place le disjoncteur différentiel de branchement 
  + Dans le labo ? C'est pas vraiment aux normes parce qu'il serait trop haut et inaccessible
  + A coté du tableau GEN ? Vérifier que la longueur avant l'amont est OK. 
    * Si c'est le cas, il faut voir ou est-ce qu'on le place sur le mur prévu pour, sachant que sur ce mur
      il y aura aussi le bouton d'arrêt d'urgence, l'alarme incendie, et une grosse boite de dérivation pour
      un Pi de monitoring du courant
- Faire un plan de l'éclairage (+ BAES), identifier précisèment les positions des BAES
  + Peut se faire en dupliquant le plan général existant, pour en retirer les prises et autres, et y placer les éclairages et (surtout) les BAES
- Revérifier l'équilibrage des phases
- Sur le plan, ajouter les répartiteurs sur les tableaux électriques
  + Pour les tableaux 2P, remplacer les répartiteurs 4P par des répartiteurs 2P
