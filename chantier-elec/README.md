# Chantier électrique

Pad collectif, utilisé pour les CRs de réunion : https://annuel2.framapad.org/p/elec-lebib-2021-9qnr

## Glossaire

Quelques terms fréquemment utilisés :
 - "Tableur", 
 - P1 : Phase 1 (le courant EDF est réparti en 3 phases, chacune sur un conducteur différent, partageant le même câble de neutre)
 - P1/P2/3 : Phase 1/2/3, pour un équipement triphasé, qui se connecte aux 3 phases
 - 2P+T : 1 phases + 1 terre, donc 3 fils (le neutre est implicite)
 - 4P+T : 3 phases + 1 terre, donc 5 fils 
 - A.U. : Bouton d'Arrêt d'Urgence
 - Boucle MX : Module qui déclenche un sectionneur quand l'A.U. est appuyé
 - C (A) : Courant max. en ampères
 - Cp (W) : Consommation permanente en watts
 - U (V) : Tension en volts
 - Prot. (mA) : Protection différentielle en milliampères

## Description générale

 - Installation d'un tableau général 63A triphasé, branché sur le compteur EDF de La Tendresse
   + Ce TGBT assurera la distribution des sous-tableaux
   + Ainsi que le branchement des circuits dans l'espace du BIB
   + Sur le tableau général, les circuits de distribution pour les tableaux des serveurs, de l'entrée, et de l'appenti n'auront qu'une protection différentielle à 300 mA
   + Sur le tableau général, les circuits prises auront tous leur propre protection différentielle 30 mA
 - Le TGBT sera installé en C2, à la place du tableau existant
 - Installation de plusieurs sous-tableaux triphasés, branchés sur le tableau général
   + Serveurs : 32A 4P (`C4` Serveurs, `B4` Régie, `A4` Studio)
   + Entrée : 32A 4P (`D0` Entrée, `D1` Couloir, `D2` Cuisine, `C0` Brasserie)
   + Appenti : 32A 2P (`D4`)
   + Laboratoire : 32A 2P (`A1`)
   + Etabli : 32A 4P (`C3`)
 - Les passages de câbles se feront majoritairement par des chemins de câbles posés au niveau des poutres et des murs.

## Zonage

Le lieu est découpé en "zones", afin de pouvoir se répérer dans l'espace :
 - A1 : Laboratoire de biologie
 - A2 : Bureau
 - A3 : Chill/bar
 - A4 : Studio
 - B1 : Impression 2D/3D et sérigraphie
 - B2 : Espace central
 - B3 : En face de la régie
 - B4 : Régie
 - C0 : Brasserie
 - C1 : Cuves à bière & frigos
 - C2 : Media
 - C3 : Etabli
 - C4 : Salle serveurs
 - D0 : Entrée
 - D1 : Couloir, toilettes, stockage
 - D2 : Brasserie
 - D4 : Appenti (atelier céramique)

Voir le plan général (plan-general.qet) pour plus d'informations.

## Tableaux électriques

L'installation est composée d'un tableau principal 63A 3P+N, codé "GEN",
placé en C2, alimenté directement depuis le disjoncteur de branchement EDF.

Ce tableau principal alimente 5 sous-tableaux divisionnaires :
 - ENT, en D2 : Couloir de l'entrée et brasserie
 - APP, en D4 : Appenti (atelier céramique)
 - LAB, en A1 : Laboratoire
 - ETA, en C3 : Etabli
 - SRV, en C4 : Salle serveurs

Tous les circuits exposés aux utilisateur.ices (lampes & éclairageà
doivent être protégés par un disjoncteur différentiel 30 mA. 
Les autres circuits sont protégés par le différentiel 300 mA présent
au niveau du disjoncteur de branchement.

L'installation comporte quelques éléments notables :
 - présence d'un arrêt d'urgence à émission de tension
   sur les tableaux GEN, LAB, ETA et SRV
 - pour toutes les zones qui ne sont pas couvertes par un 
   tableau divisionnaire, gestion de l'éclairage par un 
   contacteur présent dans le tableau GEN
 - circuits spécifiques pour la ventilation
   et pour l'éclairage de secours
 - distribution 3P+N pour tous les tableaux divisionnaires
 - protection différentielle dédiée sur la plupart des 
   circuits de prises


### Courbes de disjoncteurs & types de différentiels

 - Le pouvoir de coupure minimum est de 6kA
   pour correspondre avec le pouvoir de 
   coupure requis indiqué sur le compteur EDF
 - Les disjoncteurs utilisés seront de courbe C,
   sauf pour certains cas spécifiques :
    * pompes brasserie, courbe D si possible
 - Les différentiels utilisés seront de type AC,
   sauf pour certains cas spécifiques ;
   * salle serveurs, type HPI si possible
     (pour éviter les déclenchements intempestifs)
   * si plaques éléctriques en cuisine, type A
   * si lave-linge, type A

#### Compatibilité des assemblages disjoncteur+différentiel 

Quand un disjoncteur différentiel est formé d'un 
assemblage d'un disjoncteur et d'un différentiel,
il faut s'assurer de la compatibilité de ces deux
éléments. Pour faire simple :
 - Merlin Gerin & Schneider Electric sont souvent compatibles,
   car Merlin Gerin a été racheté par Schneider Electric
 - Les autres marques ne le sont pas

Etant donné que nous avons beaucoup de matériel Merlin Gerlin 
en stock, nous allons principalement utiliser et commander du
matériel Schneider Electric ou Merlin Gerin.

Ces considérations sont aussi importantes quand on cherche à 
utiliser des peignes pour raccorder une rangée de disjoncteurs.

## Documents de travail

Les documents se trouvent dans le dossier [/chantier-elec](./chantier-elec).

 - [Plan unifilaire général](./plan-general.qet)
   + *S'ouvre avec le logiciel QElectroTech*
   + Décrit l'organisation du TGBT et des tableaux divisionnaires
   + Décrit le placement des prises au sein de l'espace
 - [Tableur circuits/phases/équipements/matériels](./tables.ods)
   + Contient plusieurs feuilles cachées importés automatiquement depuis la sortie de `update_data.sh`
   + Contient aussi une feuille ("BoM (statique)") pour indiquer le matériel supplémentaire
   + Contient aussi une feuille permettant d'indiquer les métadonnées des composants :
     + Marque, modèle, fournisseur, référence, prix, stock actuel
   * Un tableau croisé synthétisant tout le matériel nécessaire
   + *S'ouvre avec LibreOffice*
   + Prévu pour importer les données depuis le plan unifilaire général
     * Exporter la nomenclature depuis QElectroTech, dans `data.csv`
     * Executer `./update_data.sh`
     + Mettre à jour le tableau croisé ("BoM (final)")
 - [Synthèse des couts](./synthese-couts.odt)
    * *S'ouvre avec le logiciel LibreOffice*
    * Synthèse des couts formattées, importée depuis le tableau croisé dynamique dans le tableur partagé
    * Version finale de la liste de matériel, utilisée pour les devis, la commande, et la réception
    * Données statiques (sauf pour les totaux et sous-totaux), à importer par copier/coller

### Plan unifilaire

Notes:
 - le nombre de "barres" sur un fil indique le nombre de phases :
   * 5 barres pour du triphasé (3P+N)
   * 3 barres pour du monophasé (3P)
 - les encadrés rouges indiquent :
   + pour un disjoncteur, le calibre du disjoncteur
   * pour tout autre équipement, le courant maximal supporté par le différentiel
 - les encadrés bleus indiquent, pour un différentiel, le seuil de déclenchement
 - les encadrés verts indiquent la ou les phases traversant le composant
   * (les circuits terminaux sont préfixés par un `>`

### `B2` Général 

![Tableau B2](./6_tableau_général.png "Plan unifilaire du tableau BIB")

### `C4` Serveurs/Régie/Studio

![Tableau C4](./4_tableau_serveurs.png "Plan unifilaire du tableau SRV")

### `D4` Appenti

![Tableau D4](./5_tableau_appenti.png "Plan unifilaire du tableau APP")

### `D2` Entrée

![Tableau D2](./2_tableau_entrée.png "Plan unifilaire du tableau ENT")

### `A1` Laboratoire

![Tableau A1](./1_tableau_laboratoire.png "Plan unifilaire du tableau LAB")

### `C3` Etabli

![Tableau C3](./3_tableau_etabli.png "Plan unifilaire du tableau ETA")

### Plan général

![Tableau C3](./7_plan_général.png "Plan général")
