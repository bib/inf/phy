# Mur de séparation avec l'appenti
 
  - Construction d'un mur en béton cellulaire (1 seul épaisseur) entre l'appenti et l'établi, à l'endroit de l'actuelle cloison en plastique.
  - Mur le plus étanche possible, enduit des deux côtés, pour maximiser l'isolation phonique
  - Installation d'une porte vitrée (double vitrage) à l'extrémité droite du mur
  - Installation d'une fenêtre verticale (double vitrage) à gauche dela porte pour laisser passer la lumière
  - Besoin de travailler un peu le sol pour donner une bonne base au mur et pour avoir la place pour les encadrures de portes et fenêtres
  - Réfléchir aux problèmes d'évacuation de l'eau aussi : que se passera-t-il en cas d'infiltration d'eau dans l'appenti ? accumulation d'eau au pied du mur ?
